﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private List<WaveConfig> waveConfigs;
    [SerializeField] private bool looping = true;

    private int waveIndex = 0;

    // uso start al posto di update, in modo da avere delay, aspettando la fine di ogni corutine
    IEnumerator Start()
    {
        do
        {

            yield return StartCoroutine(SpawnAllWaves());

        } while (looping);
    }

    private IEnumerator SpawnAllWaves()
    {
        waveIndex = UnityEngine.Random.Range(0, waveConfigs.Count);
        yield return SpawnWave(waveConfigs[waveIndex]);
    }

    private IEnumerator SpawnWave(WaveConfig wave)
    {
        for (int i = 0; i < wave.NumberOfEnemies; i++)
        {
            GameObject enemy = Instantiate(wave.Enemy) as GameObject;
            enemy.GetComponent<EnemyPathing>().WaveConfig = wave;
            yield return new WaitForSeconds(wave.TimeBetweenSpawns);
        }
    }
}
