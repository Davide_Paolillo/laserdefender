﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Wave Config")]
public class WaveConfig : ScriptableObject
{
    [Header("Wave Objects")]
    [SerializeField] private GameObject enemy;
    [SerializeField] private GameObject path;

    [Header("Wave Parameters")]
    [SerializeField] float timeBetweenSpawns = 0.5f;
    [SerializeField] float spawnRandomFactor = 0.3f;
    [SerializeField] int numberOfEnemies = 5;
    [SerializeField] float movementSpeed = 2.0f;

    public GameObject Enemy { get => enemy; }
    public float TimeBetweenSpawns { get => timeBetweenSpawns; }
    public float SpawnRandomFactor { get => spawnRandomFactor; }
    public int NumberOfEnemies { get => numberOfEnemies; }
    public float MovementSpeed { get => movementSpeed; }

    public List<Transform> GetWaypoints()
    {
        var waveWaypoints = new List<Transform>();
        foreach (Transform child in path.transform)
        {
            waveWaypoints.Add(child);
        }
        return waveWaypoints;
    }
}
