﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour
{
    private WaveConfig waveConfig;
    private List<Transform> waypoints;
    private int waypointsIndex;

    public WaveConfig WaveConfig { get => waveConfig; set => waveConfig = value; }

    void Start()
    {
        waypoints = waveConfig.GetWaypoints();
        waypointsIndex = 0;
        this.transform.position = new Vector3(waypoints[waypointsIndex].transform.position.x, waypoints[waypointsIndex].transform.position.y, waypoints[waypointsIndex].transform.position.z);
    }

    void Update()
    {
        PathFindMovement();
    }

    private void PathFindMovement()
    {
        if (waypointsIndex < waypoints.Count)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, waypoints[waypointsIndex].transform.position, waveConfig.MovementSpeed * Time.deltaTime);
            if (this.transform.position == waypoints[waypointsIndex].transform.position) // lo faccio alla fine, altrimenti per l'ultimo waypoint sarei out of range
            {
                waypointsIndex++;
            }
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
