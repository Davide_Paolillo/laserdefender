﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Stats")]
    [SerializeField] private float health = 100;
    [SerializeField] private float fireRate = 1.0f;
    [SerializeField] private float fireRateRandomizeFactor = 0.5f;
    [SerializeField] private float enemyPointsPerKill = 10.0f;

    [Header("Enemy VFX")]
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject explosionEffect;

    [Header("Enemy SFX")]
    [SerializeField] private AudioClip deathSound;
    [Range(0.0f, 1.0f)] [SerializeField] private float deathSoundVolume = 0.5f;

    private bool isRunning = false;
    private ScoreUpdate scoreUpdate;

    private void Start()
    {
        scoreUpdate = GameObject.FindObjectOfType<ScoreUpdate>();
    }

    private void Update()
    {
        CheckHealth();
        ShootBullets();
    }

    private void ShootBullets()
    {
        if (!isRunning)
        {
            StartCoroutine(Shoot());
            isRunning = true;
        }
    }

    private IEnumerator Shoot()
    {
        GameObject shot = Instantiate(bullet, new Vector3(this.transform.position.x, this.transform.position.y - 0.8f, this.transform.position.z), this.transform.rotation) as GameObject;
        yield return new WaitForSeconds(UnityEngine.Random.Range(fireRate - fireRateRandomizeFactor, fireRate + fireRateRandomizeFactor));
        isRunning = false;
    }

    private void CheckHealth()
    {
        if (health <= 0)
        {
            scoreUpdate.UpdateScore(enemyPointsPerKill);
            Die();
        }
    }

    private void Die()
    {
        Destroy(this.gameObject);
        AudioSource.PlayClipAtPoint(deathSound, this.transform.position, deathSoundVolume);
        GameObject particles = Instantiate(explosionEffect, this.transform.position, this.transform.rotation) as GameObject;
        Destroy(particles, 1.0f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.BULLET_TAG))
        {
            DamageDealer damageDealer = collision.gameObject.GetComponent<DamageDealer>();
            HitProcess(damageDealer);
        }
    }

    private void HitProcess(DamageDealer damageDealer)
    {
        health -= damageDealer.Damage;
        damageDealer.Hit();
    }
}
