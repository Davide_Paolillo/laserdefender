﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [Header("Enemy Bullet Stats")]
    [SerializeField] private float bulletVelocity = 20.0f;
    [SerializeField] private float bulletDmg = 100.0f;

    [Header("Enemy Bullet SFX")]
    [SerializeField] private AudioClip[] audioClip;

    public float BulletDmg { get => bulletDmg; }

    private void Start()
    {
        PlaySoundEffects();
    }

    private void PlaySoundEffects()
    {
        if (this.transform.position.x < Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x && this.transform.position.x > Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x)
        {
            AudioSource.PlayClipAtPoint(audioClip[UnityEngine.Random.Range(0, audioClip.Length)], this.transform.position);
        }
    }

    void Update()
    {
        UpdateBulletPosition();
    }

    private void UpdateBulletPosition()
    {
        var deltaY = Time.deltaTime * bulletVelocity;
        var newYPos = this.transform.position.y - deltaY;
        this.transform.position = new Vector2(this.transform.position.x, newYPos);
        CheckPositionInTheSpace();
    }

    private void CheckPositionInTheSpace()
    {
        if (this.transform.position.y < Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).y)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.BULLET_TAG) && !this.gameObject.tag.Equals(GlobalVar.BOMB_TAG))
        {
            collision.gameObject.GetComponent<DamageDealer>().Hit();
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.PLAYER_TAG))
        {
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.ENEMY_BULLET_TAG) && !this.gameObject.tag.Equals(GlobalVar.BOMB_TAG))
        {
            Destroy(this.gameObject);
        }
    }
}
