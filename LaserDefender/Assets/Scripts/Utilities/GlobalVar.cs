﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalVar
{
    private static string BULLET = "Bullet";
    private static string ENEMY_BULLET = "EnemyBullet";
    private static string PLAYER = "Player";
    private static string ENEMY = "Enemy";
    private static string LEVEL_ONE = "GameWorld";
    private static string SCORE_TEXT = "ScoreText";
    private static string HEALTH_TEXT = "HealthText";
    private static string BOMB = "Bomb";

    public static string BULLET_TAG { get => BULLET; }
    public static string ENEMY_BULLET_TAG { get => ENEMY_BULLET; }
    public static string PLAYER_TAG { get => PLAYER; }
    public static string ENEMY_TAG { get => ENEMY; }
    public static string LEVEL_ONE_SCENE { get => LEVEL_ONE; }
    public static string SCORE_TEXT_TAG { get => SCORE_TEXT; }
    public static string HEALTH_TEXT_TAG { get => HEALTH_TEXT; }
    public static string BOMB_TAG { get => BOMB; }
}
