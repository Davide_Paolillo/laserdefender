﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreUpdate : MonoBehaviour
{
    private float score;

    public float Score { get => score; }

    private void Awake()
    {
        score = 0;
    }

    private void Update()
    {
        if (GameObject.FindGameObjectWithTag(GlobalVar.SCORE_TEXT_TAG))
        {
            GameObject.FindGameObjectWithTag(GlobalVar.SCORE_TEXT_TAG).GetComponent<TextMeshProUGUI>().text = score.ToString();
        }
    }

    public void UpdateScore(float pointsToAdd)
    {
        score += pointsToAdd;
    }
    
    public void ResetScore()
    {
        score = 0;
    }
}
