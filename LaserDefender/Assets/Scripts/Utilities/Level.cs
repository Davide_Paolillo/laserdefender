﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    [SerializeField] private float delay = 2.0f;

    private int totalScene = 0;

    public void LoadNextScene()
    {
        totalScene = SceneManager.sceneCountInBuildSettings - 1;
        if (SceneManager.GetActiveScene().buildIndex < totalScene)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            Destroy(FindObjectOfType<Singleton>().gameObject);
            SceneManager.LoadScene(0);
        }
    }

    public void LoadNextSceneDelayed()
    {
        StartCoroutine(DelayedLoad());
    }

    private IEnumerator DelayedLoad()
    {
        yield return new WaitForSeconds(delay);
        LoadNextScene();
    }

    public void LoadGameScene()
    {
        GameObject.FindObjectOfType<ScoreUpdate>().ResetScore();
        SceneManager.LoadScene(GlobalVar.LEVEL_ONE_SCENE);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
