﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundScroller : MonoBehaviour
{
    [SerializeField] private float backgroundScrollSpeed = 0.2f;

    private Material myMaterial;
    private Vector2 offset;

    void Start()
    {
        myMaterial = GetComponent<Renderer>().material;
        //offset = new Vector2(myMaterial.mainTextureOffset.x + backgroundScrollSpeed, myMaterial.mainTextureOffset.y + backgroundScrollSpeed);
    }

    void Update()
    {
        offset = new Vector2(myMaterial.mainTextureOffset.x, myMaterial.mainTextureOffset.y + (backgroundScrollSpeed * Time.deltaTime));
        myMaterial.mainTextureOffset = offset;
    }
}
