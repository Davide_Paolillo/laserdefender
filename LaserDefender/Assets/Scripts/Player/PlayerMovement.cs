﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Player Movement Stats")]
    [Range(10, 100)] [SerializeField] private float moveSpeed = 100.0f;

    [Header("Padding")]
    [Range(0.1f, 1.0f)] [SerializeField] private float xLeftPadding = 0.8f;
    [Range(0.1f, 1.0f)] [SerializeField] private float xRightPadding = 0.8f;
    [Range(0.1f, 2.0f)] [SerializeField] private float yBottomPadding = 1.2f;
    [Range(0.1f, 1.0f)] [SerializeField] private float yTopPadding = 0.6f;

    private float xMin;
    private float xMax;
    private float yMin;
    private float yMax;

    void Start()
    {
        SetUpMoveBoundaries();
    }

    private void SetUpMoveBoundaries()
    {
        xMin = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + xLeftPadding; // ci permette di capire dove si trova il punto piu' in basso a sx nel mondo, ovvero il punto (0,0,0)
        xMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)).x - xRightPadding; // ci permette di capire dove si trova il punto piu' in alto a dx nel mondo, ovvero il punto (1,1,0)
        yMin= Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + yBottomPadding;
        yMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)).y - yTopPadding;
    }

    void Update()
    {
        Move();
    }

    // TODO Add boosters to movement velovity
    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed; // GetAxis restituisce un valore compreso tra 1 e -1 dove 1 e' destra a velocita' max e -1 sinista a velocita' max
        var newXPos = this.transform.position.x + deltaX;
        newXPos = Mathf.Clamp(newXPos, xMin, xMax);
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;
        var newYPos = this.transform.position.y + deltaY;
        newYPos = Mathf.Clamp(newYPos, yMin, yMax);
        this.transform.position = new Vector2(newXPos, this.transform.position.y);
        this.transform.position = new Vector2(this.transform.position.x, newYPos);
    }
}
