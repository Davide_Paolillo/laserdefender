﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Configuration param
    [Header("Player Stats")]
    [Range(100.0f, 1000.0f)] [SerializeField] private float health = 500.0f;

    [Header("Player Bullet")]
    [SerializeField] private GameObject bullet;
    [Range(0.1f, 1.0f)] [SerializeField] private float fireRate = 0.2f;

    [Header("Enemy Collisions")]
    [Range(0.0f, 100.0f)] [SerializeField] private float hitEnemyDamage = 50.0f;

    [Header("SFX")]
    [SerializeField] private AudioClip audioClip;

    private Level level;

    private bool isRunning = false;
    private bool shooted = false;
    
    private void Start()
    {
        level = GameObject.FindObjectOfType<Level>();
    }

    void Update()
    {
        if (CheckHealth()) // TODO Add perks when life is low
        {
            Fire(); // TODO implement different types of shots based on the key pressed
        }
    }

    private void Fire()
    {
        if (Input.GetAxis("Fire1") >= 1)
        {
            if (!isRunning)
            {
                isRunning = true;
                StartCoroutine(ShootBullet());
            }
        }
    }

    private bool CheckHealth()
    {
        if (health <= 0)
        {
            Die();
            level.LoadNextSceneDelayed();
            return false;
        }
        return true;
    }

    private void Die()
    {
        Destroy(this.gameObject);
        AudioSource.PlayClipAtPoint(audioClip, this.transform.position);
    }

    private IEnumerator ShootBullet()
    {
        if (!shooted)
        {
            GameObject shot = Instantiate(bullet, new Vector3(this.transform.position.x, this.transform.position.y + 0.8f), this.transform.rotation) as GameObject;
            shooted = true;
        }
        yield return new WaitForSeconds(fireRate);
        shooted = false;
        isRunning = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.ENEMY_BULLET_TAG) || collision.gameObject.tag.Equals(GlobalVar.BOMB_TAG))
        {
            EnemyBullet enemyBullet = collision.gameObject.GetComponent<EnemyBullet>();
            HitProcess(enemyBullet);
            UpdateHealthText();
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            health -= hitEnemyDamage; // TODO Refactor
            UpdateHealthText();
        }
    }

    private void HitProcess(EnemyBullet enemyBullet)
    {
        this.health -= enemyBullet.BulletDmg;
    }

    private void UpdateHealthText()
    {
        if (health < 0)
        {
            health = 0;
            GameObject.FindGameObjectWithTag(GlobalVar.HEALTH_TEXT_TAG).GetComponent<TextMeshProUGUI>().text = health.ToString();
        }
        else
        {
            GameObject.FindGameObjectWithTag(GlobalVar.HEALTH_TEXT_TAG).GetComponent<TextMeshProUGUI>().text = health.ToString();
        }
    }
}
