﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [Range(10, 100)] [SerializeField] private int bulletVelocity = 20;

    private Player player;
    private AudioSource audioSource;

    private float yMax;

    void Start()
    {
        yMax = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0)).y + 2.0f;
        player = GameObject.FindObjectOfType<Player>();
    }

    void Update()
    {
        if (!GameObject.FindObjectOfType<Player>())
        {
            Destroy(this.gameObject);
            return;
        }
        Shot();
        CheckHitOrMiss();
    }

    private void CheckHitOrMiss()
    {
        if (this.transform.position.y >= yMax)
        {
            Destroy(this.gameObject);
        }
    }

    private void Shot()
    {
        var deltaY = Time.deltaTime * bulletVelocity + (player.transform.position.y >= this.transform.position.y ? player.transform.position.y * Time.deltaTime * (-0.2f) : Time.deltaTime * (0.6f));
        var newYPos = this.transform.position.y + deltaY;
        this.transform.position = new Vector2(this.transform.position.x, newYPos);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.BOMB_TAG))
        {
            Destroy(this.gameObject);
        }
    }
}
